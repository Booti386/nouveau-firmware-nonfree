#!/bin/bash

VERSIONS=(325.15 340.32)

export old_pwd="${PWD}"
export src_dir="$(realpath $(dirname "$0"))"

function sig_handler {
	cd "${old_pwd}"
	rm -rf /tmp/nouveau
	exit 0
}

trap sig_handler SIGINT SIGQUIT SIGTERM

# wget https://raw.github.com/imirkin/re-vp2/master/extract_firmware.py -O "${src_dir}/extract_firmware.py"

mkdir /tmp/nouveau
cd /tmp/nouveau
mkdir nv
mkdir firmwares

for ver in ${VERSIONS[@]}; do

	cd /tmp/nouveau/nv

	wget -c "https://us.download.nvidia.com/XFree86/Linux-x86/${ver}/NVIDIA-Linux-x86-${ver}.run" -O ../nv-setup.run

	sh ../nv-setup.run --extract-only
	python2 "${src_dir}/extract_firmware.py"

	cp nv* vuc-* /tmp/nouveau/firmwares/

	rm -rf $(find /tmp/nouveau/nv -mindepth 1 -maxdepth 1)

done

sudo -E -H bash -c '
	install -m 755 "${src_dir}/nouveau-firmware" /usr/share/initramfs-tools/hooks/

	mkdir -m 755 -p /lib/firmware/nouveau
	install -m 644 /tmp/nouveau/firmwares/* /lib/firmware/nouveau/

	update-initramfs -u -k all
'

cd "${old_pwd}"
rm -rf /tmp/nouveau
